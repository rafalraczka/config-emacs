;;; config-dired.el --- Dired configuration -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://codeberg.org/rafalraczka/emacs-config

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;###autoload (eval-after-load "dired" '(require 'config-dired))

;;;###autoload
(require 'config)

;;;###autoload
(config-install-package 'async)

;;;###autoload
(config-install-package 'dired-collapse)

;;;###autoload
(config-install-package 'dired-ranger)

;;;###autoload
(config-install-package 'dired-single)

;;;###autoload
(config-install-package 'git-annex-el)

(require 'config-env)
(require 'dired)

(require 'git-annex)

(defun config-dired-do-delete-permanently (&optional arg)
  (interactive "P")
  (let ((delete-by-moving-to-trash nil))
    (dired-do-delete arg)))

(defun config-dired-create-dwim (path)
  (interactive (list (read-file-name "Create: " (dired-current-directory))))
  (if (directory-name-p path)
      (dired-create-directory path)
    (dired-create-empty-file path)))

(defun config-dired-enable-truncate-lines ()
  (setq truncate-lines t))

(defun config-dired-rename-adding-prefix (prefix &optional arg)
  "Rename current file or all marked (or next ARG) files adding PREFIX.
With non-zero prefix argument ARG, the command operates on the
next ARG files.  Otherwise, it operates on all the marked files
or the current file if none are marked.

This command also renames any buffers that are visiting the files."
  (interactive (list (read-string "File prefix: ")
                     current-prefix-arg))
  (dired-do-rename-regexp "^\\(.\\)" (concat prefix "\\1") arg))

(add-hook 'dired-after-readin-hook #'config-dired-enable-truncate-lines)

;; Use different approach to group directories first when =ls= is not
;; available.  This also set default switches.

(defvar ls-lisp-dirs-first)             ; Defined in ls-lisp.el.

(if (executable-find "ls")
    (setq dired-listing-switches "-Ahlv --group-directories-first")
  (progn (setq dired-listing-switches "-Ahlv")
         (setq ls-lisp-dirs-first t)))

(when config-env-android
  (add-hook 'dired-mode-hook #'dired-hide-details-mode))

(declare-function dired-async-mode "ext:dired-async")

(add-hook 'dired-mode-hook #'dired-async-mode)

(declare-function dired-collapse-mode "ext:dired-collapse")

(add-hook 'dired-mode-hook #'dired-collapse-mode)

(let ((map dired-mode-map))
  (keymap-set map "e" 'config-dired-create-dwim)
  (keymap-set map "C-w" 'dired-ranger-copy)
  (keymap-set map "C-y" 'dired-ranger-move)
  (keymap-set map "M-y" 'dired-ranger-paste)
  (keymap-substitute map 'dired-find-file 'dired-single-buffer)
  (keymap-substitute map 'dired-up-directory 'dired-single-up-directory))

;;; Footer:

(provide 'config-dired)

;;; config-dired.el ends here
