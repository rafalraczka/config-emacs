;;; config-etc.el --- Configuration for miscellaneous features -*- lexical-binding: t; -*-

;; Copyright (C) 2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;###autoload
(progn

(require 'config)

(config-install-package 'desktop-environment)
(config-install-package 'geiser)
(config-install-package 'geiser-guile)
(config-install-package 'guix)
(config-install-package 'hide-mode-line)
(config-install-package 'linux-app)
(config-install-package 'speed-type)
(config-install-package 'visual-fill)
(config-install-package 'visual-fill-column)

;;; Footer:

(provide 'config-etc))

;;; config-etc.el ends here
