;;; config-which-key.el --- which-key configuration -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://codeberg.org/rafalraczka/emacs-config

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;###autoload
(require 'config)

;;;###autoload
(config-install-package 'which-key)

;;;###autoload
(require 'config-util)

(defvar which-key-idle-delay)

;;;###autoload
(setf which-key-idle-delay 0.5)

(defvar which-key-lighter)

;;;###autoload
(setf which-key-lighter nil)

(defvar which-key--buffer)

;;;###autoload
(progn (defun config-center-which-key-buffer ()
  "Center the text of the entire buffer."
  (when (and which-key--buffer (get-buffer-window which-key--buffer t))
    (config-center-buffer which-key--buffer))))

;;;###autoload
(advice-add 'which-key--show-page :after #'config-center-which-key-buffer)

(defvar which-key-mode nil)

(declare-function which-key-mode "which-key")

;;;###autoload (add-hook 'after-init-hook #'which-key-mode)
(unless which-key-mode (which-key-mode))

;;; Footer:

;;;###autoload
(provide 'config-which-key)

;;; config-which-key.el ends here
