;;; config-util.el --- Utility functions -*- lexical-binding: t; -*-

;; Copyright (C) 2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;###autoload
(progn (defun config--line-width ()
  "Return width of line at point in pixels."
  (let* ((string (buffer-substring
                  (line-beginning-position) (line-end-position)))
         (width (frame-char-width))
         (len (string-pixel-width string)))
    (+ (/ len width)
       (if (zerop (% len width)) 0 1)))))

;;;###autoload
(progn (defun config--max-line-width (start end)
  "Return width of the widest line between START to END, in pixels."
  (save-excursion
    (goto-char start)
    (let ((width 0))
      (while (< (point) end)
        (let ((current-line-width (config--line-width)))
          (if (> current-line-width width)
              (setf width current-line-width)))
        (forward-line 1))
      width))))

;;;###autoload
(progn (defun config-center-buffer (buffer)
  "Center the text in BUFFER."
  (when (buffer-live-p buffer)
    (with-current-buffer buffer
      (let* ((width (config--max-line-width (point-min) (point-max)))
             (prefix
              (propertize
               " " 'display
               `(space . (:align-to (- center ,(/ (float width) 2)))))))
        (add-text-properties
         (point-min) (point-max)
         `(line-prefix ,prefix indent-prefix ,prefix)))))))

;;; Footer:

;;;###autoload
(provide 'config-util)

;;; config-util.el ends here
