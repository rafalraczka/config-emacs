;;; config-spell-checking.el --- Spell checking mode -*- lexical-binding: t; -*-

;; Copyright (C) 2024-2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'cl-lib))

;;;###autoload
(require 'config)

;;;###autoload
(require 'config-env)

(defvar config-spell-checking-spell-checker)

;;;###autoload
(cond
 ((locate-file "jinx.el" load-path)
  (setf config-spell-checking-spell-checker 'jinx))
 ((executable-find "hunspell")
  (setf config-spell-checking-spell-checker 'flyspell)
  (config-install-package 'flyspell)))

(defgroup config-spell-checking nil
  "Spell checking in buffers."
  :prefix "config-spell-checking-"
  :group 'config)

(defvar jinx-languages)

(setf jinx-languages "en_GB-ize pl_PL")

(defvar ispell-dictionary)

(setf ispell-dictionary "en_GB-ize")

(defvar global-config-spell-checking-modes
  '(text-mode prog-mode conf-mode))

(defvar config-spell-checking-mode)

(declare-function jinx-mode "ext:jinx")

(defvar-keymap config-spell-checking-mode-map
  :doc "Keymap for Config-Spell-Checking mode.")

(pcase config-spell-checking-spell-checker
  ('jinx
   (let ((map config-spell-checking-mode-map))
     (keymap-set map "M-$" 'jinx-correct))))

;;;###autoload
(define-minor-mode config-spell-checking-mode
  "Check spelling in a buffer."
  :group 'config-spell-checking
  :keymap 'config-spell-checking-mode-map
  (let ((mode (pcase config-spell-checking-spell-checker
                ('flyspell
                 (if (derived-mode-p 'text-mode)
                     'flyspell-mode
                   'flyspell-prog-mode))
                ('jinx
                 'jinx-mode))))
    (if config-spell-checking-mode
        (funcall mode 1)
      (funcall mode -1))))

(defun config-spell-checking--on ()
  (if (and (not (or noninteractive
                    buffer-read-only
                    (buffer-base-buffer)
                    (eq (aref (buffer-name) 0) ?\s)))
           (or (eq t global-config-spell-checking-modes)
               (cl-loop
                for p in global-config-spell-checking-modes
                thereis (if (and (symbolp p)
                                 (not (or (null p) (eq p t))))
                            (derived-mode-p p)
                          (user-error
                           "Invalid value in `%S': %S"
                           'config-spell-checking--spell-checker
                           p)))))
      (config-spell-checking-mode)))

;;;###autoload
(define-globalized-minor-mode global-config-spell-checking-mode
  config-spell-checking-mode config-spell-checking--on
  :group 'config-spell-checking)

;;; Footer:

(provide 'config-spell-checking)

;;; config-spell-checking.el ends here
