;;; config-help.el --- Help interface -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://codeberg.org/rafalraczka/emacs-config

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;###autoload (setf features (delq 'help features))
;;;###autoload (eval-after-load 'help '(require 'config-help))

;;;###autoload
(require 'config)

;;;###autoload
(config-install-package 'helpful)

(require 'help)

(defun config-help-set-display-buffer-action ()
  (let ((f-width  (frame-width))
        (condition "\\*Help.*"))
    (push (cons condition
                (if (< f-width 80)
                    'display-buffer-same-window
                  `((display-buffer-in-side-window)
                    (side . left)
                    (slot . -1)
                    (window-width . ,(if (< f-width 160) .5 80)))))
          display-buffer-alist)))

(eval-after-load 'help '(config-help-set-display-buffer-action))

;;;###autoload
(defvar-keymap config-help-map
  :parent 'help-command
  "f" 'helpful-callable
  "v" 'helpful-variable
  "k" 'helpful-key)

;;;###autoload
(fset 'config-help-map config-help-map)

;;; Footer:

(provide 'config-help)

;;; config-help.el ends here
