;;; config.el --- Modular configuration -*- lexical-binding: t; -*-

;; Copyright (C) 2023-2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://codeberg.org/rafalraczka/emacs-config

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:



;;; Variables.

(defvar config--after-init-eval-forms nil)

(defvar config-after-first-interaction-hook-time nil
  "Value of `current-time' after first interaction hook.")

(defvar config-before-first-interaction-hook-time nil
  "Value of `current-time' before first interaction hook.")

(defvar config-disabled-config-libraries nil)

(defvar config-first-interaction-hook nil
  "Normal hook run before first interaction in the Emacs session.
The first interaction is considered to be the first command call
or first `find-file' non-interactive call.")


;;; Customization groups.

(defgroup config nil
  "Personal Emacs configuration."
  :group 'emacs)


;;; Generating loaddef files.

(defun config-disable-config-library (library)
  (add-to-list 'config-disabled-config-libraries
               (concat (symbol-name library) ".el")))

(defun config-generate-loaddefs (output-file &optional excluded-files)
  (setf excluded-files (append excluded-files config-disabled-config-libraries))
  (let* ((dir (if load-file-name
                  (file-name-directory load-file-name)
                default-directory))
         (backup-inhibited t)
         (find-file-hook nil)
         (inhibit-message t)
         (message-log-max nil)
         (write-file-functions nil)
         (disable-string (format ";; Excluded: %S" excluded-files))
         rebuild)
    (if (file-exists-p output-file)
        (with-temp-buffer
          (insert-file-contents output-file)
          (unless (re-search-forward (concat "^" disable-string "$") nil t)
            (setf rebuild t))))
    (loaddefs-generate dir (expand-file-name output-file dir)
                       excluded-files disable-string nil rebuild)))


;;; Package managers.

;;;; package.el.

(defvar package-archives)

(defun config--initialize-package-el ()
  (require 'package)
  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
  (package-initialize))

(defvar config--package-contents-refreshed nil)

(defun config--package-ensure (package)
  "Ensure that PACKAGE is installed with package.el."
  (or (package-installed-p package)
      (condition-case err
          (package-install package)
        (error
         (if config--package-contents-refreshed
             (signal (car err) (cdr err)))))
      (progn (package-refresh-contents)
             (setf config--package-contents-refreshed t)
             (package-install package))))

;;;; straight.el.

(defvar bootstrap-version)

(defun config--initialize-straight-el ()
  (let ((bootstrap-file (expand-file-name
                         "straight/repos/straight.el/bootstrap.el"
                         (or (bound-and-true-p straight-base-dir)
                             user-emacs-directory)))
        (bootstrap-version 7))
    (unless (file-exists-p bootstrap-file)
      (with-current-buffer
          (url-retrieve-synchronously
           "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
           'silent 'inhibit-cookies)
        (goto-char (point-max))
        (eval-print-last-sexp)))
    (load bootstrap-file nil 'nomessage)))

(defvar straight--success-cache)

(declare-function straight-use-package "ext:straight")

(defun config--straight-ensure (package)
  (unless (and straight--success-cache
               (gethash (symbol-name package) straight--success-cache))
    (straight-use-package package)))


;;; Installing packages.

(defvar config--package-manager nil)

(defconst config-supported-package-managers '(package-el straight-el))

;;;###autoload
(defun config-initialize-package-manager (package-manager)
  (when config--package-manager
      (if (eq package-manager config--package-manager)
          (warn "Package manager already initialized: %s" package-manager))
      (error "Once initialized, package manager cannot be changed"))
  (unless (symbolp package-manager)
    (signal 'wrong-type-argument (list 'symbolp package-manager)))
  (unless (memq package-manager config-supported-package-managers)
    (error "Package manager not supported: %S" package-manager))
  (pcase package-manager
    ('package-el (config--initialize-package-el))
    ('straight-el (config--initialize-straight-el)))
  (setf config--package-manager package-manager))

(defvar config--ensured-packages (make-hash-table :size 120))

;;;###autoload
(defun config-install-package (package &rest _)
  (unless config--package-manager
    (config-initialize-package-manager 'package-el)
    (warn "Package manager not chosen, fallback to default (%s)" 'package-el))
  (unless (gethash package config--ensured-packages)
    (pcase config--package-manager
      ('package-el (config--package-ensure package))
      ('straight-el (config--straight-ensure package)))
    (puthash package t config--ensured-packages)))


;;; Hooks and other features which call or evaluate on special occasions.

(defun config-run-first-interaction-hook ()
  "Run `config-first-interaction-hook'."
  (unless config-before-first-interaction-hook-time
    (setq config-before-first-interaction-hook-time (current-time)))
  (let* ((current-fun 0)
         (len (length config-first-interaction-hook))
         (reporter (make-progress-reporter "Running first interaction hook: "
                                           current-fun len)))
    (run-hook-wrapped
     'config-first-interaction-hook
     (lambda (fun &rest args)
       (setq current-fun (1+ current-fun))
       (progress-reporter-update reporter current-fun (format "%S" fun))
       (apply fun args)
       nil))
    (remove-hook 'find-file-hook #'config-run-first-interaction-hook)
    (remove-hook 'pre-command-hook #'config-run-first-interaction-hook)
    (progress-reporter-done reporter))
  (unless config-after-first-interaction-hook-time
    (setq config-after-first-interaction-hook-time (current-time))))

(add-hook 'find-file-hook #'config-run-first-interaction-hook -50)
(add-hook 'pre-command-hook #'config-run-first-interaction-hook -50)

(defmacro config-add-one-time-hook (hook function &optional depth local)
  (let ((hook-function
         (intern
          (format "config--%s-%s--one-time-hook"
                  (eval hook) (eval function)))))
    `(progn
       (defun ,hook-function ()
         (remove-hook ,hook ',hook-function ,local)
         (funcall ,function))
       (add-hook ,hook ',hook-function ,depth ,local))))

(defun config-eval-after-init (form)
  (if after-init-time
      (eval form)
    (add-to-list 'config--after-init-eval-forms form)))

(defun config--run-eval-after-init-forms ()
  (mapc #'eval config--after-init-eval-forms))

(add-hook 'after-init-hook 'config--run-eval-after-init-forms -90)


;;; Benchmarking configuration.

(defvar config-after-startup-time nil
  "Value of `current-time' after `after-init' hooks.")

(defvar config-gcs-done-during-init nil
  "Number of garbage collections done after the Emacs initialization.")

(defvar config-gcs-done-during-startup nil
  "Number of garbage collections done after the Emacs startup.")

(defvar config-gcs-done-during-first-interaction-hook nil
  "Number of garbage collections done during first interaction hook.")

(defun config-benchmark ()
  "Return benchmark of the Emacs startup."
  (interactive)
  (let ((init-time (float-time (time-subtract after-init-time before-init-time)))
	(startup-time (float-time (time-subtract config-after-startup-time
                                                 before-init-time)))
        (f-i-time (when config-after-first-interaction-hook-time
                    (float-time
                     (time-subtract
                      config-after-first-interaction-hook-time
                      config-before-first-interaction-hook-time)))))
    (message (concat "         Emacs startup time         \n"
	             "                                    \n"
	             "| stage       | time [s] | gcs     |\n"
	             "+-------------+----------+---------+\n"
	             "| init:       | %.2e | %.1e |\n"
	             "| after init: | %.2e | %.1e |\n"
	             "+-------------+----------+---------+\n"
	             "| startup:    | %.2e | %.1e |"
                     (when f-i-time
                       (concat "\n"
                               "+-------------+----------+---------+\n"
                               "| f-i hook    | %.2e |    -    |")))
             init-time
             config-gcs-done-during-init
             (- startup-time init-time)
             (- config-gcs-done-during-startup config-gcs-done-during-init)
             startup-time
             config-gcs-done-during-startup
             f-i-time)))

(defun config-check-emacs-version (ver)
  "Give warning about old Emacs if `emacs-version' is lower than VER."
  (when (version< emacs-version ver)
    (warn "%s %s\n  %s %s %s"
          "Your Emacs version is" emacs-version
	  "this configuration was only tested with version" ver "or higher")))

(defun config-set-after-startup-time ()
  "Set `config-after-startup-time' value to the value of `current-time'."
  (unless config-after-startup-time
    (setq config-after-startup-time (current-time))))

(defun config-set-init-gcs-done ()
  "Set `config-gcs-done-during-init' value to the current value of `gcs-done'."
  (unless config-gcs-done-during-init
    (setq config-gcs-done-during-init gcs-done)))

(defun config-set-startup-gcs-done ()
  "Set `config-gcs-done-during-startup' value to the current value of `gcs-done'."
  (unless config-gcs-done-during-startup
    (setq config-gcs-done-during-startup gcs-done)))

(add-hook 'after-init-hook #'config-set-init-gcs-done -100)
(add-hook 'emacs-startup-hook #'config-benchmark 100)
(add-hook 'emacs-startup-hook #'config-set-after-startup-time 99)
(add-hook 'emacs-startup-hook #'config-set-startup-gcs-done 99)

;;; Footer:

(provide 'config)

;;; config.el ends here
