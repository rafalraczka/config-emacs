;;; config-undo-tree.el --- undo-tree.el configuration -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://codeberg.org/rafalraczka/emacs-config

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;###autoload
(require 'config)

;;;###autoload
(config-install-package 'undo-tree)

(declare-function global-undo-tree-mode "ext:undo-tree")

;;;###autoload
(add-hook 'config-first-interaction-hook #'global-undo-tree-mode)

(defvar undo-tree-auto-save-history)    ; Defined in undo-tree.el.

;;;###autoload
(setq undo-tree-auto-save-history nil)

;;; Footer:

;;;###autoload
(provide 'config-undo-tree)

;;; config-undo-tree.el ends here
