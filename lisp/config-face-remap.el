;;; config-face-remap.el --- face-remap configuration -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://codeberg.org/rafalraczka/emacs-config

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;###autoload (eval-after-load "face-remap" '(require 'config-face-remap))

;;;###autoload
(defcustom config-face-remap-variable-pitch-height-multiplier 1.05
  "The height of the `variable-pitch' as a multiplier of the `default' face height."
  :type 'number
  :group 'display)


;; TODO: Modify this approach to be local instead of global.
;;;###autoload
(defface config-face-remap-fixed-pitch-copy '((t))
  "Copy of the `fixed-pitch' face.")

;;;###autoload
(copy-face 'fixed-pitch 'config-face-remap-fixed-pitch-copy)

;;;###autoload
(progn
(defun config-face-remap-set-default-fixed-pitch-height ()
  (custom-theme-set-faces
   'user
   '(fixed-pitch ((t :height 1.0)))))

(defun config-face-remap-set-relative-pitch-height (&optional multiplier)
  (let* ((multi (or multiplier config-face-remap-variable-pitch-height-multiplier))
         (default-height (face-attribute 'default :height))
         (fixed-height-multiplier (/ 1 multi))
         (variable-family (face-attribute 'variable-pitch :family))
         (fixed-family (face-attribute 'default :family))
         (variable-height (round (* default-height multi))))
    (custom-theme-set-faces
     'user
     `(variable-pitch ((t (:family ,variable-family :height ,variable-height))))
     `(fixed-pitch ((t (:family ,fixed-family :height ,fixed-height-multiplier)))))))

(defvar buffer-face-mode)               ; Defined in face-remap.el.
(defvar buffer-face-mode-face)          ; Defined in face-remap.el.

(defun config-face-remap-toggle-relative-height ()
  (if (and buffer-face-mode
           (string-equal buffer-face-mode-face "variable-pitch"))
      (config-face-remap-set-relative-pitch-height)
    (config-face-remap-set-default-fixed-pitch-height))))

;;;###autoload
(add-hook 'buffer-face-mode-hook #'config-face-remap-toggle-relative-height)

;;; Footer:

;;;###autoload
(provide 'config-face-remap)

;;; config-face-remap.el ends here
