;;; config-calendar.el --- calendar.el configuration -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://codeberg.org/rafalraczka/emacs-config

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;###autoload (eval-after-load "calendar" '(require 'config-calendar))

(defvar calendar-left-margin)

(setf calendar-left-margin 2)

(defvar calendar-date-style)

(setopt calendar-date-style 'iso)

(defvar calendar-minimum-window-height)

(setf calendar-minimum-window-height 9)

(defvar calendar-week-start-day)

(setf calendar-week-start-day 1)

;; By default, calendar is displayed at the bottom of the frame or in the
;; other window if more then one window is in the current frame and tab
;; (side windows do not count).  I prefer to have consistent behaviour and
;; have it always at the bottom of the frame.

(setf (alist-get "\\*Calendar\\*" display-buffer-alist nil nil 'equal)
      '((display-buffer-in-direction)
        (direction . bottom)))

(defvar olivetti-global-modes nil)

(add-to-list 'olivetti-global-modes 'calendar-mode)

(defun config-calendar-mode-config ()
  (setq-local fill-column 71))

(add-hook 'calendar-mode-hook #'config-calendar-mode-config)

;;; Footer:

(provide 'config-calendar)

;;; config-calendar.el ends here
