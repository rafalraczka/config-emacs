;;; config-modus-themes.el --- Modus themes configuration -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://codeberg.org/rafalraczka/emacs-config

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;###autoload
(progn

(require 'config)

(config-install-package 'modus-themes)

(require 'modus-themes)

(defvar config-modus-themes-default-theme (if (display-graphic-p)
                                              'modus-operandi
                                            'modus-vivendi)
  "Default theme to load.")

(defun config-modus-themes-load-theme (&optional theme)
  (interactive (list (intern (completing-read "Theme: "
                                              '(modus-operandi modus-vivendi)
                                              nil 'require-match))))
  (let ((theme (or theme config-modus-themes-default-theme 'operandi)))
    (pcase theme
      ('modus-operandi
       (modus-themes-load-theme 'modus-operandi))
      ('modus-vivendi
       (modus-themes-load-theme 'modus-vivendi))
      (_ (error "Wrong theme specified %s" theme)))
    (config-modus-themes-set-faces)))

(defvar citar-indicator-cited)          ; Defined in citar.el.
(defvar citar-indicator-files)          ; Defined in citar.el.
(defvar citar-indicator-function)       ; Defined in citar.el.
(defvar citar-indicator-links)          ; Defined in citar.el.
(defvar citar-indicator-notes)          ; Defined in citar.el.
(defvar citar-indicators)          ; Defined in citar.el.
(defvar citar-symbols)                  ; Defined in citar.el.
(defvar modus-operandi-palette-overrides) ; Defined in modus-operandi-theme.el.
(defvar modus-themes-org-blocks)        ; Defined in modus-themes.el.
(defvar modus-vivendi-palette-overrides) ; Defined in modus-vivendi-theme.el.
(defvar org-priority-faces)             ; Defined in org-faces.el.
(defvar org-todo-keyword-faces)         ; Defined in org-faces.el.

(declare-function citar-indicator-create "ext:citar")
(declare-function citar-indicator-function "ext:citar")
(declare-function citar-indicator-symbol "ext:citar")
(declare-function citar-indicator-tag "ext:citar")

(defun config-modus-themes-set-faces ()
  "Set faces which overrides `modus-themes' defaults."
  (cond
   ((memq 'modus-vivendi custom-enabled-themes)
    (config-modus-themes-set-faces-vivendi))
   ((memq 'modus-operandi custom-enabled-themes)
    (config-modus-themes-set-faces-operandi)))
  (set-face-attribute 'fringe nil :inherit 'line-number :background 'unspecified)
  (set-face-attribute 'mode-line nil :box 'unspecified)
  (set-face-attribute 'mode-line-inactive nil :box nil)
  (set-face-attribute 'tab-bar nil
                      :background 'unspecified
                      :box 'unspecified
                      :inherit 'mode-line-inactive)
  (set-face-attribute 'tab-bar-tab nil
                      :background 'unspecified
                      :box 'unspecified
                      :height 0.75
                      :inherit 'mode-line)
  (set-face-attribute 'tab-bar-tab-inactive nil
                      :background 'unspecified
                      :box 'unspecified
                      :inherit 'mode-line-inactive
                      :height 0.75)
  (with-eval-after-load 'citar
    (setf citar-indicator-cited
          (citar-indicator-create
           :symbol (propertize (citar-indicator-symbol citar-indicator-cited)
                               'face '(:inherit modus-themes-fg-green))
           :function (citar-indicator-function citar-indicator-cited)
           :tag (citar-indicator-tag citar-indicator-cited))
          citar-indicator-files
          (citar-indicator-create
           :symbol (propertize (citar-indicator-symbol citar-indicator-files)
                               'face '(:inherit modus-themes-fg-magenta))
           :function (citar-indicator-function citar-indicator-files)
           :tag (citar-indicator-tag citar-indicator-files))
          citar-indicator-links
          (citar-indicator-create
           :symbol (propertize (citar-indicator-symbol citar-indicator-links)
                               'face '(:inherit modus-themes-fg-blue))
           :function (citar-indicator-function citar-indicator-links)
           :tag (citar-indicator-tag citar-indicator-links))
          citar-indicator-notes
          (citar-indicator-create
           :symbol (propertize (citar-indicator-symbol citar-indicator-notes)
                               'face '(:inherit modus-themes-fg-cyan))
           :function (citar-indicator-function citar-indicator-notes)
           :tag (citar-indicator-tag citar-indicator-notes)))
    (setf (nth 0 citar-indicators)
          citar-indicator-cited
          (nth 1 citar-indicators)
          citar-indicator-files
          (nth 2 citar-indicators)
          citar-indicator-links
          (nth 3 citar-indicators)
          citar-indicator-notes))
  (with-eval-after-load 'diff-mode
    (set-face-attribute 'diff-refine-added nil :weight 'bold)
    (set-face-attribute 'diff-refine-changed nil :weight 'bold)
    (set-face-attribute 'diff-refine-removed nil :weight 'bold))
  (with-eval-after-load 'org-faces
    (set-face-attribute 'org-agenda-date-today nil :background 'unspecified)
    (set-face-attribute 'org-cite nil :inherit 'default)
    (set-face-attribute 'org-headline-done nil :inherit 'shadow)
    (set-face-attribute 'org-scheduled nil :foreground 'unspecified)
    (set-face-attribute 'org-tag nil :foreground nil :inherit 'shadow)
    (setq org-priority-faces
          `((?A . (:inherit modus-themes-fg-magenta-intense))
            (?B . (:inherit modus-themes-fg-magenta))
            (?C . (:inherit default))
            (?D . (:inherit modus-themes-fg-blue))
            (?E . (:inherit modus-themes-fg-blue-faint))))))

(defun config-modus-themes-set-faces-operandi ()
  (with-eval-after-load 'olivetti
    (set-face-attribute 'olivetti-fringe nil :background 'unspecified)))

(defun config-modus-themes-set-faces-vivendi ()
  "Set faces which overrides Modus Vivendi defaults."
  (set-face-attribute 'line-number nil :background "#1e1e1e")
  (set-face-attribute 'mode-line-inactive nil :background "#100f10")
  (set-face-attribute 'vertical-border nil :foreground "#1e1e1e")
  (with-eval-after-load 'org-faces
    (set-face-attribute 'org-block nil :background "#252525")
    (set-face-attribute 'org-block-begin-line nil :background "#323232")
    (set-face-attribute 'org-cite-key nil :foreground "#00d3d0"))
  (with-eval-after-load 'org-ref-ref-links
    (set-face-attribute 'org-ref-ref-face nil :foreground "#00d3d0")))

(setq modus-operandi-palette-overrides '((bg-completion bg-active)
                                         (bg-mode-line-active bg-active)
                                         (border bg-main)
                                         (comment yellow-warmer)
                                         (warning yellow-intense)))
(setq modus-themes-org-blocks 'gray-background)
(setq modus-vivendi-palette-overrides '((bg-main "#191919")
                                        (bg-inactive "#373737")
                                        (bg-tab-active "#323232")
                                        (bg-tab-inactive "#100f10")
                                        (comment yellow-faint)))

(if (daemonp)
    (add-hook 'server-after-make-frame-hook #'config-modus-themes-load-theme)
  (add-hook 'after-init-hook #'config-modus-themes-load-theme))

;;; Footer:

(provide 'config-modus-themes))

;;; config-modus-themes.el ends here
