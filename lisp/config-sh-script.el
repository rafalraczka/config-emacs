;;; config-sh-script.el --- Configuration for shell-script editing -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2023, 2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://codeberg.org/rafalraczka/emacs-config

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;###autoload (eval-after-load "sh-script" '(require 'config-sh-script))

;;;###autoload
(require 'config)

;;;###autoload
(config-install-package 'lsp-mode)

(require 'lsp-mode)

(add-hook 'sh-mode-hook #'lsp-deferred)

(if (fboundp 'config-tree-sitter-maybe-enable)
    (config-tree-sitter-maybe-enable 'bash 'sh-mode 'bash-ts-mode))

;;; Footer:

(provide 'config-sh-script)

;;; config-sh-script.el ends here
