;;; config-prog-mode.el --- Prog mode configuration -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://codeberg.org/rafalraczka/emacs-config

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;###autoload
(require 'config)

;;;###autoload
(config-install-package 'flycheck)

;;;###autoload
(config-install-package 'rainbow-delimiters)

;;;###autoload
(config-install-package 'smartparens)

(require 'config-env)
(require 'config-spell-checking)

(defvar olivetti-body-width)            ; Defined in olivetti.el.

(defvar config-prog-mode-fill-column 80
  "Default `fill-column' for programming modes.")

(defvar config-prog-mode-comment-fill-column 72
  "Default `fill-column' for programming modes.")

(defvar config-prog-mode-comment-column 40
  "Default `comment-column' for programming modes.")

(defvar olivetti-body-width)

(declare-function flycheck-mode "ext:flycheck")
(declare-function rainbow-delimiters-mode "ext:rainbow-delimiters")
(declare-function smartparens-mode "ext:smartparens")

;;;###autoload
(defun config-prog-mode-config ()
  (setq-local comment-auto-fill-only-comments t)
  (setq-local comment-fill-column config-prog-mode-comment-fill-column)
  (setf comment-column config-prog-mode-comment-column)
  (setf fill-column config-prog-mode-fill-column)
  (auto-fill-mode)
  (display-line-numbers-mode)
  (flycheck-mode)
  (outline-minor-mode)
  (rainbow-delimiters-mode)
  (smartparens-mode))

;;;###autoload
(config-eval-after-init '(add-hook 'prog-mode-hook #'config-prog-mode-config))

(unless global-config-spell-checking-mode (global-config-spell-checking-mode))

;;; Footer:

(provide 'config-prog-mode)

;;; config-prog-mode.el ends here
