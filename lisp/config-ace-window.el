;;; config-ace-window.el --- ace-window configuration -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://codeberg.org/rafalraczka/emacs-config

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;###autoload (eval-after-load "ace-window" '(require 'config-ace-window))

;;;###autoload
(require 'config)

;;;###autoload
(config-install-package 'ace-window)

(require 'ace-window)
(require 'config-env)

(setq aw-dispatch-when-more-than 1)

(if config-env-exwm-required
    (setq aw-scope 'frame)
  (setq aw-scope 'visible))

(setf aw-dispatch-alist
      '((?x aw-delete-window "Delete Window")
        (?m aw-swap-window "Swap Windows")
        (?M aw-move-window "Move Window")
        (?c aw-copy-window "Copy Window")
        (?j aw-switch-buffer-in-window "Select Buffer")
        (?f aw-flip-window)
        (?b aw-switch-buffer-other-window "Switch Buffer Other Window")
        (?e aw-execute-command-other-window "Execute Command Other Window")
        (?F aw-split-window-fair "Split Fair Window")
        (?2 aw-split-window-vert "Split Vert Window")
        (?3 aw-split-window-horz "Split Horz Window")
        (?1 delete-other-windows "Delete Other Windows")
        (?T aw-transpose-frame "Transpose Frame")
        ;; ?i ?r ?t are used by hyperbole.el
        (?? aw-show-dispatch-help)))

(setf aw-keys '(?s ?e ?t ?n ?r ?i ?d ?h ?a ?o))

;;; Footer:

(provide 'config-ace-window)

;;; config-ace-window.el ends here
