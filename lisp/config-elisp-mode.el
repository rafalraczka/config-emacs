;;; config-elisp-mode.el --- Emacs Lisp configuration -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2023, 2025 Rafał Rączka <info@rafalraczka.com>
;;
;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://codeberg.org/rafalraczka/emacs-config

;; This file is NOT part of GNU Emacs.
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;###autoload
(require 'config)

;;;###autoload (defun config-elisp-mode--require ()
;;;###autoload   (require 'config-elisp-mode))

;;;###autoload (config-eval-after-init
;;;###autoload  '(config-add-one-time-hook
;;;###autoload    'emacs-lisp-mode-hook 'config-elisp-mode--require))

(require 'elisp-mode)


;;; checkdoc.

(defvar checkdoc-force-docstrings-flag)

(setf checkdoc-force-docstrings-flag nil)

(defvar checkdoc-verb-check-experimental-flag)

(setf checkdoc-verb-check-experimental-flag nil)


;;; Flycheck.

(defvar flycheck-emacs-lisp-load-path)

(setf flycheck-emacs-lisp-load-path 'inherit)


;;; Keymap: `emacs-lisp-mode-map'.

(let ((map emacs-lisp-mode-map))
  (keymap-set map "C-c C-c" 'eval-buffer))

;;; Footer:

(provide 'config-elisp-mode)

;;; config-elisp-mode.el ends here
