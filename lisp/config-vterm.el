;;; config-vterm.el --- Vterm configuration -*- lexical-binding: t; -*-

;; Copyright (C) 2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;###autoload (eval-after-load "vterm" '(require 'config-vterm))

;;;###autoload
(require 'config)

;;;###autoload
(unless (locate-file "vterm.el" load-path)
  (config-install-package 'vterm))

(require 'vterm)


;;; Keymap: `vterm-mode-map'.

(let ((map vterm-mode-map))
  (keymap-set map "C-q" 'vterm-send-next-key)
  (keymap-unset map "<f5>")
  (keymap-unset map "<f6>")
  (keymap-unset map "<f7>")
  (keymap-unset map "<f8>")
  (keymap-unset map "<f9>"))

;;; Footer:

(provide 'config-vterm)

;;; config-vterm.el ends here
