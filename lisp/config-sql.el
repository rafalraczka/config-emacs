;;; config-sql.el --- Configuration for SQL -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;###autoload (eval-after-load "sql" '(require 'config-sql))

;;;###autoload
(require 'config)

;;;###autoload
(config-install-package 'sql-indent)

;;;###autoload
(config-install-package 'lsp-mode)


;;; sql-indent.

(defvar config-sql-sqlind-additional-indentation-offsets-alist
  '((create-statement config-sql-sqlind-lone-semicolon +)
    (delete-clause 0)
    (in-delete-clause + sqlind-lone-semicolon)
    (in-select-clause sqlind-lone-semicolon +)
    (in-update-clause + sqlind-right-justify-logical-operator
                      sqlind-lone-semicolon)
    (nested-statement-continuation config-sql-sqlind-indent-special)
    (select-clause 0)
    (select-join-condition +)
    (select-table-continuation sqlind-use-anchor-indentation +)
    (update-clause 0)))

(defvar sqlind-basic-offset)

(setq-default sqlind-basic-offset 4)

(defun config-sql-sqlind-lone-semicolon (_syntax base-indentation)
  (save-excursion
    (back-to-indentation)
    (if (looking-at ";")
        (indent-line-to 0)
      base-indentation)))

(declare-function sqlind-use-anchor-indentation "ext:sql-indent")

(defun config-sql-sqlind-indent-special (syntax base-indentation)
  (save-excursion
    (back-to-indentation)
    (let ((offset
           (if (looking-at (concat
                            "\\(foreign key\\)"
                            "\\|\\(match\\)"
                            "\\|\\(on delete\\)"
                            "\\|\\(on update\\)"
                            "\\|\\(primary key\\)"
                            "\\|\\(references\\)"
                            ))
               (* 2 sqlind-basic-offset)
             sqlind-basic-offset)))
      (+ syntax base-indentation offset))))

(defvar sqlind-default-indentation-offsets-alist)

(defun config-sql-sqlind-add-to-indentation-offsets-alist (&optional alist)
  (interactive)
  (let ((element (or alist
                     config-sql-sqlind-additional-indentation-offsets-alist)))
    (when element
      (let ((alist `(,@element
                     ,@sqlind-default-indentation-offsets-alist)))
        (setq-default sqlind-indentation-offsets-alist alist)))))

(advice-remove 'sqlind-lone-semicolon 'config-sql-sqlind-lone-semicolon)

(add-hook 'sqlind-minor-mode-hook
          #'config-sql-sqlind-add-to-indentation-offsets-alist)


;;; LSP (sqls).

(declare-function olivetti-mode "ext:olivetti")

(defun config-sql-lsp-sqls-config-results-buffer ()
  "Configure buffer with results of the sql query."
  (when (string-equal (buffer-name) "*sqls results*")
    (setq truncate-lines t)
    (when (featurep 'olivetti)
      (olivetti-mode -1))))

(declare-function lsp-deferred "ext:lsp-mode")

(add-hook 'help-mode-hook #'config-sql-lsp-sqls-config-results-buffer 50)
(add-hook 'sql-mode-hook #'lsp-deferred)

(defvar lsp-sqls-workspace-config-path)

(setf lsp-sqls-workspace-config-path nil)

;;; Footer:

(provide 'config-sql)

;;; config-sql.el ends here
