;;; config-my-polybar.el --- Polybar utilities -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2023, 2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://codeberg.org/rafalraczka/emacs-config

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'xdg)

(defvar config-my-polybar--process nil)

(defvar config-my-polybar-command "polybar")

(defvar config-my-polybar-configurations
  `((default
     :launch-script ,(expand-file-name "polybar/launch.sh"  (xdg-config-home))
     :description "Default configuration")))

(defun config-my-polybar--configuration-cmd-and-args (configuration)
  (let ((launch-script (plist-get configuration :launch-script)))
    (if (and launch-script
             (file-executable-p launch-script))
        (list launch-script)
      (let ((cmd (or (plist-get configuration :polybar-command)
                     config-my-polybar-command)))
        (if (and cmd (setf cmd (executable-find cmd)))
            (let ((bar-name (plist-get configuration :bar))
                  (config-file (plist-get configuration :config-file)))
              (append (list cmd)
                      (if bar-name
                          (list bar-name))
                      (if config-file
                          (list (format "--config=%S" config-file)))))
          (error "There is no command available to start polybar"))))))

(defun config-my-polybar-send-hook (name number)
  (call-process
   "polybar-msg" nil nil nil
   (list "action" (format "#%s.hook.%s" name number))))

;;;###autoload
(defun config-my-polybar-kill ()
  "Kill polybar process."
  (interactive)
  (if config-my-polybar--process
      (prog2
          (ignore-errors (kill-process config-my-polybar--process))
          config-my-polybar--process
        (setf config-my-polybar--process nil))
    (user-error
     "No polybar process or polybar not started with config-my-polybar")))

;;;###autoload
(defun config-my-polybar-start ()
  "Start polybar process."
  (interactive)
  (let* ((config (alist-get 'default config-my-polybar-configurations))
         (cmd-and-args (config-my-polybar--configuration-cmd-and-args config)))
    (when config-my-polybar--process
      (config-my-polybar-kill)
      (sleep-for (expt 10 -3)))
    (setf config-my-polybar--process
          (apply #'start-process "polybar" "*polybar-process*" cmd-and-args))))

;;;###autoload
(defalias 'config-my-polybar-restart 'config-my-polybar-start)

;;;###autoload
(defun config-my-polybar-toggle-panel ()
  "Enable polybar if disabled or disable if enabled."
  (interactive)
  (if config-my-polybar--process
      (config-my-polybar-kill)
    (config-my-polybar-start)))

;;; Footer:

(provide 'config-my-polybar)

;;; config-my-polybar.el ends here
