;;; config-text-mode.el --- Text mode configuration -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2023, 2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://codeberg.org/rafalraczka/emacs-config

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'config-env)
(require 'config-spell-checking)

(defgroup config-text-mode nil
  "Text mode configuration."
  :group 'config)

(defcustom config-text-mode-fill-column 72
  "Default `fill-column' for Text mode."
  :type 'integer
  :group 'config-text-mode)

;;;###autoload
(defun config-text-mode-config ()
  (setq-local fill-column config-text-mode-fill-column))

;;;###autoload
(add-hook 'text-mode-hook #'config-text-mode-config)

(unless global-config-spell-checking-mode (global-config-spell-checking-mode))

;;; Footer:

(provide 'config-text-mode)

;;; config-text-mode.el ends here
