;;; config-emacs.el --- Configuration for essential features -*- lexical-binding: t; -*-

;; Copyright (C) 2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;###autoload
(progn

(require 'config)

(require 'server)


;;; Auto-saving and backup files.

(setf backup-by-copying t)
(setf delete-old-versions t)
(setf kept-new-versions 6)
(setf kept-old-versions 2)
(setf version-control t)

;; Emacs create backups and auto-save files in directories of that files by
;; default.  It can cause cluttering and it can require additional work to
;; configure different software to ignore backup files and auto-saves.
;; Because of that it can be desired then to keep these files in single
;; directory, independent of file location.

;; Following code will keep all backup and auto-save files in directory of the
;; Emacs configuration.  In case user use Chemacs, Chemacs directory will be
;; preferred to keep auto-saves and backups in the same place for various
;; Emacs profiles.

;; Sources
;; - https://emacstragic.net/uncategorized/editing-files-using-sudo-and-emacs/

(let ((dir (or config-env-chemacs-directory user-emacs-directory)))
  (setf auto-save-file-name-transforms`((".*" ,(concat dir "auto-saves/") t)))
  (setf backup-directory-alist `(("." . ,(concat dir "backups/")))))

;; Create directories for auto-save files.

(dolist (a auto-save-file-name-transforms)
  (let ((dir (nth 1 a)))
    (when (not (file-exists-p dir))
      (mkdir dir))))

;;;; super-save.

(config-install-package 'super-save)

(defvar super-save-auto-save-when-idle)

(setf super-save-auto-save-when-idle t)

(declare-function super-save-mode "ext:super-save")

(add-hook 'config-first-interaction-hook #'super-save-mode)


;;; Customization stored in `custom-file'.

(setf custom-file
      (expand-file-name "config-custom-file.el" user-emacs-directory))

(progn (defun config-custom-load-custom-file ()
  (load custom-file 'no-error 'no-message)))

(add-hook 'after-init-hook #'config-custom-load-custom-file -50)


;;; Emacs server.

(progn (defun config-server-ensure-server ()
  "Ensure that the server is running."
  (unless (server-running-p)
    (server-start))))

(add-hook 'after-init-hook #'config-server-ensure-server)


;;; Faces.

(set-face-attribute 'default nil :family "JuliaMono" :height 110)


;;; Filesystem.

(setf delete-by-moving-to-trash t)

(config-add-one-time-hook 'find-file-hook #'global-auto-revert-mode)


;;; Frame.

(setf visual-line-fringe-indicators `(left-curly-arrow ,nil))

(declare-function config-my-mini-gui-mode "config-mini-gui")

(if (daemonp)
    (add-hook 'server-after-make-frame-hook #'config-my-mini-gui-mode)
  (add-hook 'after-init-hook #'config-my-mini-gui-mode))


;;; Garbage collection.

(config-install-package 'gcmh)

(declare-function gcmh-mode "ext:gcmh")

(add-hook 'after-init-hook #'gcmh-mode)


;;; Input.

(advice-add 'yes-or-no-p :override 'y-or-n-p)


;;; Minibuffer.

(setf enable-recursive-minibuffers t)

(add-hook 'config-first-interaction-hook #'savehist-mode)


;;; Mode line.

(config-install-package 'minions)

(defvar minions-mode-line-lighter)

(setf minions-mode-line-lighter "*")

(add-hook 'config-first-interaction-hook #'column-number-mode)

(declare-function minions-mode "ext:minions")

(add-hook 'after-init-hook #'minions-mode)


;;; Startup.

;; `initial-major-mode' can be set to change the major mode of the *scratch*
;; buffer.

(setf initial-major-mode 'fundamental-mode)

;; Changing content of the message for the scratch buffer.

(setf initial-scratch-message ";; Scratch\n\n")

;; Preventing startup screen for displaying as the initial screen for
;; Emacs session.

(setf inhibit-startup-screen t)

(defvar config-emacs-scratch-buffer-setup-hook nil)

(defun config-emacs--setup-scratch-buffer ()
  (when-let ((buf (get-buffer "*scratch*")))
    (advice-remove 'get-scratch-buffer-create 'config-emacs--setup-scratch-buffer)
    (with-current-buffer buf
      (run-hooks 'config-emacs-scratch-buffer-setup-hook)
      (add-hook 'kill-buffer-hook
                (lambda () (advice-add 'get-scratch-buffer-create
                                       :after 'config-emacs--setup-scratch-buffer))
                nil t))))

(add-hook 'config-emacs-scratch-buffer-setup-hook 'lisp-interaction-mode)

(advice-add 'get-scratch-buffer-create :after 'config-emacs--setup-scratch-buffer)


;;; TRAMP (transparent remote file access).

(defvar tramp-default-method)

(setf tramp-default-method "ssh")


;;; Visual feedback.

(config-install-package 'volatile-highlights)

(defvar config-emacs-blink-mode-line-bg-color nil)

(defvar config-emacs-blink-mode-line-bg-default-color "#ff0000")

(defun config-emacs-blink-mode-line-bg (&optional color)
  (let ((blink-color (or color
                         config-emacs-blink-mode-line-bg-color
                         (when (facep 'ansi-color-red)
                           (face-attribute 'ansi-color-red :background))
                         config-emacs-blink-mode-line-bg-default-color))
        (original-color (face-background 'mode-line)))
    (set-face-background 'mode-line blink-color)
    (run-with-idle-timer 0.1 nil
                         (lambda (col) (set-face-background 'mode-line col))
                         original-color)))

(setf ring-bell-function #'config-emacs-blink-mode-line-bg)

(declare-function volatile-highlights-mode "ext:volatile-highlights")

(add-hook 'config-first-interaction-hook #'volatile-highlights-mode)


;;; Essential configuration for buffer editing.

(config-install-package 'hl-todo)
(config-install-package 'ws-butler)

(setq-default fill-column 80)

(setq-default indent-tabs-mode nil)

(setf tab-always-indent 'complete)


;;;; Cleanup of non-visible characters.

(declare-function ws-butler-global-mode "ext:ws-butler")

(config-add-one-time-hook 'find-file-hook #'ws-butler-global-mode)

;;;; EditorConfig.

(config-install-package 'editorconfig)

(defvar editorconfig-mode-lighter)

(setf editorconfig-mode-lighter nil)

(declare-function editorconfig-mode "ext:editorconfig")

(config-add-one-time-hook 'find-file-hook #'editorconfig-mode)

;;;; Keywords highlight.

(defvar hl-todo-highlight-punctuation)

(setf hl-todo-highlight-punctuation ":")

(declare-function hl-todo-mode "ext:hl-todo")

(add-hook 'conf-mode-hook #'hl-todo-mode)
(add-hook 'prog-mode-hook #'hl-todo-mode)
(add-hook 'text-mode-hook #'hl-todo-mode)

;;; Footer:

(provide 'config-emacs))

;;; config-emacs.el ends here
