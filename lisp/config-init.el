;;; config-init.el --- Modular configuration -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://codeberg.org/rafalraczka/emacs-config

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'config)

(config-check-emacs-version "29.4")

(require 'config-env)
(require 'config-straight)

(config-initialize-package-manager 'straight-el)

(config-generate-loaddefs
 "config-loaddefs.el"
 '("config.el" "config-init.el" "config-env.el" "config-straight.el"
   "config-loaddefs.el"))

(require 'config-loaddefs)

;;; Footer:

(provide 'config-init)

;;; config-init.el ends here
