;;; config-straight.el --- Configuration for straight.el package manager -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://codeberg.org/rafalraczka/emacs-config

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(defvar config-straight-profile 'config)

(defvar config-straight-recipes
  '((git-email
     :host sourcehut :repo "~yoctocell/git-email"
     :files ("git-email.el" "git-email-magit.el" "git-email-notmuch.el"))
    (git-timemachine
     :host codeberg :repo "pidu/git-timemachine")
    (linux-app
     :type git :flavor melpa :host codeberg :repo "rafalraczka/emacs-linux-app"
     :branch "main")
    (lilypond
     :files ("elisp/*.el"))
    (olivetti
     :type git :flavor melpa :host github :repo "rnkn/olivetti"
     :fork (:host codeberg :repo "rafalraczka/olivetti" :branch "main"))
    (org-noter
     :type git :flavor melpa
     :host github :repo "weirdNox/org-noter"
     :fork (:host codeberg :repo "rafalraczka/org-noter" :branch "main"))
    (org-pdftools
     :type git :flavor melpa :host github :repo "fuxialexander/org-pdftools"
     :fork (:host codeberg :repo "rafalraczka/org-pdftools" :branch "main"))
    (org-ref-prettify
     :type git :flavor melpa :host github :repo "alezost/org-ref-prettify.el"
     :fork (:host codeberg :repo "rafalraczka/org-ref-prettify.el"
            :branch "main"))
    (org-tree-slide
     :type git :flavor melpa :host github :repo "takaxp/org-tree-slide"
     :fork (:host codeberg :repo "rafalraczka/org-tree-slide" :branch "main"))
    (pico-dashboard
     :host codeberg :repo "rafalraczka/pico-dashboard" :branch "devel"
     :files (:defaults "banners"))
    (qutebrowser
     :host github :repo "lrustand/qutebrowser.el")
    (screenshot
     :type git :host github :repo "tecosaur/screenshot" :bulid (:not compile)
     :fork (:host codeberg :repo "rafalraczka/screenshot" :branch "main"))
    (speed-type
     :type git :host github :repo "parkouss/speed-type"
     :fork (:host codeberg :repo "rafalraczka/speed-type" :branch "main"))
    (sql-indent
     :type git :host github :repo "alex-hhh/emacs-sql-indent"
     :files ("*" (:exclude ".git"))
     :fork (:host codeberg :repo "rafalraczka/emacs-sql-indent" :branch "main"))
    (yasnippet
     :fork (:host codeberg :repo "rafalraczka/yasnippet" :branch "main"))))

(defvar config-straight-version-lockfile-name
  (format "%s.el" config-straight-profile))

(defvar straight-current-profile)

(setf straight-current-profile config-straight-profile)

(defvar straight-enable-use-package-integration)

(setf straight-enable-use-package-integration nil)

;; =straight= is slower in comparison to built-in =package=.  To improve
;; initialization time with =straight= it is recommended to change value of
;; the `straight-check-for-modifications' variable.  Following code change its
;; value to the recommended one in official =straight= repository.
;; https://github.com/raxod502/straight.el/blob/master/README.md#my-init-time-got-slower

(defvar straight-check-for-modifications)

(if (executable-find "watchexec")
    (setf straight-check-for-modifications '(watch-files find-when-checking))
  (setf straight-check-for-modifications '(check-on-save find-when-checking)))

(defvar straight-recipe-overrides nil)

(setf (alist-get config-straight-profile straight-recipe-overrides)
      config-straight-recipes)

(defvar straight-profiles '((nil . "default.el")))

(push (cons config-straight-profile config-straight-version-lockfile-name)
      straight-profiles)

;;; Footer:

(provide 'config-straight)

;;; config-straight.el ends here
