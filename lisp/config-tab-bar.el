;;; config-tab-bar.el --- Tab Bar configuration -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2023, 2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://codeberg.org/rafalraczka/emacs-config

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;###autoload (defun config-tab-bar--require () (require 'config-tab-bar))
;;;###autoload (config-add-one-time-hook
;;;###autoload  'tab-bar-mode-hook 'config-tab-bar--require)

(setq tab-bar-auto-width t)
(setq tab-bar-auto-width-max '(150 20))
(setq tab-bar-back-button nil)
(setq tab-bar-close-button-show nil)
(setq tab-bar-format '(tab-bar-format-history
                       tab-bar-format-tabs
                       tab-bar-separator))
(setq tab-bar-forward-button nil)
(setq tab-bar-show 1)
(setq tab-bar-tab-name-function 'tab-bar-tab-name-truncated)
(setq tab-bar-tab-name-truncated-max 20)

;;; Footer:

(provide 'config-tab-bar)

;;; config-tab-bar.el ends here
