;;; config-transient.el --- Transient interface configuration -*- lexical-binding: t; -*-

;; Copyright (C) 2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;###autoload (eval-after-load "transient" '(require 'config-transient))

;;;###autoload
(require 'config)

;;;###autoload
(config-install-package 'transient)

(require 'config-util)
(require 'transient)

(defun config-center-transient-buffer ()
  "Center the text between START and END."
  (if (window-live-p transient--window)
      (config-center-buffer transient--buffer)))

(eieio-oset-default 'transient-column 'pad-keys t)

(advice-add 'transient--show :after 'config-center-transient-buffer)

;;; Footer:

(provide 'config-transient)

;;; config-transient.el ends here
