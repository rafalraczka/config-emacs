;;; config-completion.el --- Completion configuration -*- lexical-binding: t; -*-

;; Copyright (C) 2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;###autoload
(require 'config)

;;;###autoload
(config-install-package 'consult)

;;;###autoload
(config-install-package 'corfu)

;;;###autoload
(config-install-package 'marginalia)

;;;###autoload
(config-install-package 'orderless)

;;;###autoload
(config-install-package 'vertico)

;;;###autoload
(require 'corfu)

(require 'marginalia)
(require 'vertico)


;;; Corfu.

;;;###autoload
(progn
(setf corfu-cycle t)
(setf corfu-auto t)
(setf corfu-auto-delay 0.1)
(setf corfu-auto-prefix 0)
(setf corfu-preview-current nil))

;; StrawberryTea (aka LemonBreezes) is the author and copyright holder of the
;; following code.  This code includes definitions of the two functions.
;; https://github.com/emacs-exwm/exwm/issues/31#issuecomment-2029704237

;; --8<---------------cut here---------------start------------->8---

;; Copyright (C) 2024 StrawberryTea (aka LemonBreezes)

;;;###autoload
(progn (defun config-completion--get-focused-monitor-geometry ()
  "Get the geometry of the monitor displaying the selected frame in EXWM."
  (let* ((monitor-attrs (frame-monitor-attributes))
         (workarea (assoc 'workarea monitor-attrs))
         (geometry (cdr workarea)))
    (list (nth 0 geometry)              ; X
          (nth 1 geometry)              ; Y
          (nth 2 geometry)              ; Width
          (nth 3 geometry)))))          ; Height

;;;###autoload
(progn (defun config-completion--advise-corfu-make-frame
    (orig-fun frame x y width height)
  "Advise `corfu--make-frame' to be monitor-aware.
ORIG-FUN is advised function Show current buffer in child frame
at X/Y with WIDTH/HEIGHT.  FRAME is the existing frame."
  ;; Get the geometry of the currently focused monitor
  (let* ((monitor-geometry (config-completion--get-focused-monitor-geometry))
         (monitor-x (nth 0 monitor-geometry))
         (monitor-y (nth 1 monitor-geometry))
         (selected-frame-position (frame-position))
         (selected-frame-x (car selected-frame-position))
         (selected-frame-y (cdr selected-frame-position))
         (new-x (+ monitor-x selected-frame-x x))
         (new-y (+ monitor-y selected-frame-y y)))
    ;; Call the original function with potentially adjusted coordinates
    (funcall orig-fun frame new-x new-y width height))))

;; --8<---------------cut here---------------end--------------->8---

;;;###autoload
(if (display-graphic-p)
    (advice-add 'corfu--make-frame :around
                #'config-completion--advise-corfu-make-frame))

;;;###autoload (add-hook 'config-first-interaction-hook 'global-corfu-mode)
(unless corfu-mode (global-corfu-mode))

;;; Keymap: `corfu-mode-map'.

;;;###autoload
(let ((map corfu-map))
  (keymap-unset map "<remap> <move-beginning-of-line>")
  (keymap-unset map "<remap> <move-end-of-line>")
  (keymap-unset map "<remap> <beginning-of-buffer>")
  (keymap-unset map "<remap> <end-of-buffer>")
  (keymap-unset map "<remap> <scroll-down-command>")
  (keymap-unset map "<remap> <scroll-up-command>")
  (keymap-unset map "<remap> <next-line>")
  (keymap-unset map "<remap> <previous-line>")
  (keymap-unset map "<down>")
  (keymap-unset map "<up>")
  (keymap-unset map "RET")
  (keymap-set map "M-RET" 'corfu-insert))


;;; Marginalia.

;;;###autoload (add-hook 'config-first-interaction-hook 'marginalia-mode)
(unless marginalia-mode (marginalia-mode))


;;; Orderless.

;;;###autoload
(setf completion-styles '(orderless basic))


;;; Vertico.

;;;###autoload (add-hook 'config-first-interaction-hook 'vertico-mode)
(unless vertico-mode (vertico-mode))

;;; Footer:

;;;###autoload
(provide 'config-completion)

;;; config-completion.el ends here
