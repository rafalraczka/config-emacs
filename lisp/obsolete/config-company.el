;;; config-company.el --- Configuration for Company package -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://codeberg.org/rafalraczka/emacs-config

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;###autoload (eval-after-load 'comapny '(require 'config-company))

;;;###autoload
(require 'config)

;;;###autoload
(config-install-package 'company)

;;;###autoload
(config-install-package 'company-quickhelp)

(defvar company-idle-delay)             ; Defined in company.el.
(defvar company-minimum-prefix-length)  ; Defined in company.el.
(defvar company-selection-wrap-around)  ; Defined in company.el.
(defvar company-tooltip-limit)          ; Defined in company.el.

(setq company-idle-delay 0)
(setq company-minimum-prefix-length 1)
(setq company-selection-wrap-around t)
(setq company-tooltip-limit 6)

(defvar company-quickhelp-delay)

(setq company-quickhelp-delay 0.25)

;;; Footer:

(provide 'config-company)

;;; config-company.el ends here
