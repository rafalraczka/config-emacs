;;; config-module.el --- Modular configuration -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2024 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://codeberg.org/rafalraczka/emacs-config

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'cl-lib))

(defvar config-module-package-contents-refreshed nil)
(defvar config-module-package-manager nil)
(defvar config-module-prefix nil)

(defun config-module-install-local-package (name)
  (let ((dir (expand-file-name (concat "lisp/" (symbol-name name))
                               user-emacs-directory)))
    (straight-use-package
     `(,name :type nil
             :local-repo ,dir))))

(defvar package-archive-contents)       ; Defined in package.el.

(defun config-module-package-ensure (package)
  "Ensure that PACKAGE is installed and return non-nil if successful."
  (or (package-installed-p package)
      (progn (if (assoc package package-archive-contents)
                 (condition-case err
                     (package-install package)
                   (error (config-module-package-refresh-maybe-and-install package)))
               (config-module-package-refresh-maybe-and-install package))
             (package-installed-p package))))

(defun config-module-package-refresh-maybe-and-install (package)
  "Refresh archive contents if not done yet and install PACKAGE."
  (when (not config-module-package-contents-refreshed)
    (package-refresh-contents)
    (setq config-module-package-contents-refreshed t))
  (package-install package))

(declare-function straight-use-package "ext:straight")

(defun config-module-install-package (package package-manager)
  (pcase package-manager
    ('straight (straight-use-package package))
    ('package (config-module-package-ensure package))
    ('local (config-module-install-local-package package))
    (_ (error "Unknown package manager: %s for package %s"
              package-manager package))))

(cl-defmacro config-module (name &key after disable eval install
                                 install-when require when)
  (declare (indent 1) (debug t))
  `(when (and (null ,disable)
              (or (null (quote ,when)) ,when))
     (let* ((prefix (symbol-name ',config-module-prefix))
            (file (format "%s-%s" prefix (symbol-name ,name)))
            (package-manager (if (or (eq t ,install) (null ,install))
                                 config-module-package-manager
                               ,install)))
       (when (or ,install ,install-when)
         (config-module-install-package ,name package-manager))
       (when ,require
         (dolist (pkg ,require)
           (config-module-install-package pkg package-manager)))
       (when ,after
         (eval-after-load ,after (lambda () (require (intern file)))))
       ,@eval)))

;;; Footer:

(provide 'config-module)

;;; config-module.el ends here
